# What is this project

This project is a webservice which calls an external API to get some weather data for a specified town.

Then, the data is stored in a .txt file and, if you try to make another call in the 15 following minutes, the data will come from the file instead of coming from the API.

Finally, the webservice returns the result in JSON.

# How to start the project

Create a .env file as shown in the sample and put your api key in

You can also change the "TIME_IN_SECONDS" variable by the integer value you want (it should be set at 900 by default which is the number of seconds in 15 minutes)

Run the app.py

Open your browser and go on a correct path (example : localhost:8080/weather?zipcode=26000)
