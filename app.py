from dotenv import load_dotenv
from datetime import datetime
import dateutil.parser
import os.path
from os import path
import requests
import os
import web
import json
import re

# get api key
load_dotenv()
key = os.getenv("API_KEY")
defined_time = os.getenv("TIME_IN_SECONDS")

# Generating routes
urls = (
    '/weather', 'HandleRequest',
    '/(.*)', 'HandleError',
)

app = web.application(urls, globals())


# Function which call the 'openweathermap' API and returns an object created with the returned data
def api_request(zipcode):
    url = str('http://api.openweathermap.org/data/2.5/weather?zip=' + zipcode + ',fr&lang=fr&appid=' + key)
    response = requests.get(url)
    dictionary = json.loads(response.text)

    final_response = {
        'temps': dictionary['weather'][0]['description'],
        'temperature': dictionary['main']['temp'],
        'minimale': dictionary['main']['temp_min'],
        'maximale': dictionary['main']['temp_max'],
        'appel': datetime.now().isoformat(),
    }

    return final_response


# Function which create a new file and implement data in it
def create_file(zipcode):
    file = open("weather.txt", "w+")

    response = api_request(zipcode)
    data = {'towns': [{zipcode: response}]}

    json.dump(data, file)
    return response


# Function which handle the data inside the weather.txt
def handle_data(zipcode):
    with open('weather.txt', 'r') as json_file:
        weather = json.load(json_file)

        for item in weather['towns']:
            if zipcode in item:
                lastCall = dateutil.parser.parse(item[zipcode]['appel'])
                now = datetime.now()
                difference = round((now - lastCall).total_seconds())

                if difference < int(defined_time):
                    return {zipcode: item[zipcode]}

                else:
                    response = api_request(zipcode)
                    item[zipcode] = response

                    with open('weather.txt', 'w') as outfile:
                        json.dump(weather, outfile)

                    return {zipcode: response}

        response = api_request(zipcode)
        response = {
            zipcode: response
        }
        weather['towns'].append(response)

        with open('weather.txt', 'w') as outfile:
            json.dump(weather, outfile)

        return response


class HandleRequest:
    # Class which handle the request on '/weather' path
    def GET(self):
        web.header('content-type', 'application/json')

        if web.input():

            if re.match(r'\d{5}$', web.input()['zipcode']):
                zipcode = web.input()['zipcode']

                # create and implement a file with data if the file doesn't exist yet
                if not path.exists("weather.txt"):
                    response = create_file(zipcode)
                    return json.dumps(response, ensure_ascii=False)

                else:
                    response = handle_data(zipcode)
                    return json.dumps(response, ensure_ascii=False)

            else:
                return json.dumps({
                    'Error': 'Invalid value for the zipcode, try something like: \'localhost:8080/weather?zipcode=44000\''
                })

        else:
            return json.dumps({
                'Error': 'No zipcode entered, try something like: \'localhost:8080/weather?zipcode=44000\''
            })


class HandleError:
    # Class which handle all the wrong path (!= '/weather')
    def GET(self, error):
        web.header('content-type', 'application/json')

        if error:
            error_string = '\'' + error + '\' is not a correct path, try something like : \'localhost:8080/weather?zipcode=44000\''

        else:
            error_string = error + 'Invalid path, try something like : \'localhost:8080/weather?zipcode=44000\''

        return json.dumps({
            'Error': error_string
        })


if __name__ == "__main__":
    app.run()
